package tw.peng.swipertest.ui.wallet

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.item_wallet_pager.view.*
import tw.peng.swipertest.R
import tw.peng.swipertest.databinding.FragmentWalletBinding


class WalletFragment : Fragment() {

    private lateinit var viewModel: ViewModel
    private var _binding: FragmentWalletBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProvider(this).get(ViewModel::class.java)

        _binding = FragmentWalletBinding.inflate(inflater, container, false)
        val root: View = binding.root

        viewModel.fakeData.observe(viewLifecycleOwner) { fakeData ->


            binding.tab.apply {
                removeAllTabs()
                fakeData.forEach {
                    addTab(newTab().apply {
//                    text = it.title
                    })
                }
            }

            binding.viewPager.apply {
                adapter = WalletAdapter(requireContext(), fakeData)
//            addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(binding.tab))
            }

//        binding.tab.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(binding.viewPager))
//        binding.tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
//            override fun onTabSelected(tab: TabLayout.Tab) {
//                binding.viewPager.currentItem = tab.position
//            }
//            override fun onTabUnselected(tab: TabLayout.Tab) {}
//            override fun onTabReselected(tab: TabLayout.Tab) {}
//        })

            binding.tab.setupWithViewPager(binding.viewPager)
        }

        viewModel.getFakeData()

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

data class FakeData(val title: String, val count: Int, val dataList: List<String>)

class WalletAdapter(private val context: Context, private val dataList: List<FakeData>) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.item_wallet_pager, container, false).apply {
            val data = dataList[position]
            dataContainer.apply {
                data.dataList.forEachIndexed { index, s ->
                    addView(TableRow(context).apply {
                        addView(TextView(context).apply {
                            layoutParams = TableRow.LayoutParams().apply { weight = 1f }
                            gravity = Gravity.CENTER
                            text = "data$index:"
                        })

                        addView(TextView(context).apply {
                            layoutParams = TableRow.LayoutParams().apply { weight = 1f }
                            gravity = Gravity.CENTER
                            text = s
                        })
                    })
                }
            }
            countText.text = data.count.toString()
            ad.text = "AD $position"
        }
        container.addView(view)
        return view
    }

    override fun getPageTitle(position: Int): CharSequence? = dataList[position].title

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) = container.removeView(`object` as View)

    override fun getCount() = dataList.size

    override fun isViewFromObject(view: View, `object`: Any) = view == `object`
}