package tw.peng.swipertest.ui.phone

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import tw.peng.swipertest.databinding.FragmentPhoneBinding

class PhoneFragment : Fragment() {

    private lateinit var viewModel: ViewModel
    private var _binding: FragmentPhoneBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProvider(this).get(ViewModel::class.java)

        _binding = FragmentPhoneBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.pageTitle
        viewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}