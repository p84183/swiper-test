package tw.peng.swipertest.ui.wallet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ViewModel : ViewModel() {

    private val fakeDataLiveData = MutableLiveData<List<FakeData>>()
    val fakeData: LiveData<List<FakeData>> = fakeDataLiveData

    fun getFakeData() {
        fakeDataLiveData.value = listOf(
            FakeData("容幣記錄", 1, listOf("666", "777", "888")),
            FakeData("兌換卷", 2, listOf("aaa", "bbb", "ccc"))
        )
    }
}