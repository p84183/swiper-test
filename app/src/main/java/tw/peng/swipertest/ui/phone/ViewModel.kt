package tw.peng.swipertest.ui.phone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is phone Fragment"
    }
    val text: LiveData<String> = _text
}